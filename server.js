var express = require("express"),
    session = require("express-session"),
    RedisStore = require("connect-redis")(session);
var cookieParser = require('cookie-parser');
    redisClient = require("redis").createClient();

var app = express();
app.use(cookieParser());
app.use(session({
    secret: "secreto",
    store: new RedisStore({
        client: redisClient
    }),
    cookie: {
        maxAge  : 24*60*60*1000
    },
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

app.get("/api/:nombre", function (req, res) {
    req.session.nombre = req.params.nombre;
    res.send(req.session.nombre);
});

app.get("/mostrar", function (req, res)  {
    res.send(req.session);
});

app.listen(3000, function() {
    console.log("Servidor corriendo en el puerto 3000");
});
