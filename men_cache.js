var express = require("express");
    session = require("express-session");
var cookieParser = require('cookie-parser');
var MemStore = express.session.MemoryStore;

var app = express();

app.use(cookieParser());
app.use(session({
    secret: "secreto",
    store: MemStore({
        reapInterval: 60000 * 10
    }),
    cookie: {
        path    : '/',
        httpOnly: false,
        maxAge  : 24*60*60*1000
    },
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

app.get("/api/:nombre", function (req, res) {
    req.session.nombre = req.params.nombre;
    res.send(req.session.nombre);
    console.log(req);
});

app.get("/mostrar", function (req, res)  {
    res.send(req.session);
    console.log(req);
});

app.listen(3000, function() {
    console.log("Servidor corriendo en el puerto 3000");
});
